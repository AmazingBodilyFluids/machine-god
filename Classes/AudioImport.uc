class AudioImport expands Object abstract;

#exec AUDIO IMPORT FILE="Sounds\Player\Cough.mp3" NAME="Cough" GROUP="Player"
#exec AUDIO IMPORT FILE="Sounds\Player\Death.mp3" NAME="Death" GROUP="Player"
#exec AUDIO IMPORT FILE="Sounds\Player\Drown.mp3" NAME="Drown" GROUP="Player"
#exec AUDIO IMPORT FILE="Sounds\Player\EyePain.mp3" NAME="EyePain" GROUP="Player"
#exec AUDIO IMPORT FILE="Sounds\Player\Gasp.mp3" NAME="Gasp" GROUP="Player"
#exec AUDIO IMPORT FILE="Sounds\Player\Jump.mp3" NAME="Jump" GROUP="Player"
#exec AUDIO IMPORT FILE="Sounds\Player\Land.mp3" NAME="Land" GROUP="Player"
#exec AUDIO IMPORT FILE="Sounds\Player\PainLarge.mp3" NAME="PainLarge" GROUP="Player"
#exec AUDIO IMPORT FILE="Sounds\Player\PainMedium.mp3" NAME="PainMedium" GROUP="Player"
#exec AUDIO IMPORT FILE="Sounds\Player\PainSmall.mp3" NAME="PainSmall" GROUP="Player"
#exec AUDIO IMPORT FILE="Sounds\Player\WaterDeath.mp3" NAME="WaterDeath" GROUP="Player"

#exec AUDIO IMPORT FILE="Sounds\Ambient\OceanDistant.wav" NAME="OceanDistant" GROUP="Ambient"
