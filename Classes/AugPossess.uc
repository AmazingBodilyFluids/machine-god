class AugPossess extends Augmentation;

var float mpAugValue;
var float mpEnergyDrain;
var ScriptedPawn TargetPawn;
var Vector HitLocation, HitNormal;
var Vector Position, Line;
var ScriptedPawn HitPawn;

var Inventory StartInventory;
var Name StartAlliance;
var Rotator StartRotation;
var Vector StartPosition;

defaultproperties
{
     mpAugValue=1.000000
     mpEnergyDrain=05.000000
     EnergyRate=300.000000
     Icon=Texture'MachineGod.UI.AugIconPossess'
     smallIcon=Texture'MachineGod.UI.AugIconPossess_Small'
     AugmentationName="Possess"
     Description="Take over people's minds, dude.|n|nTECH ONE: Power drain is normal.|n|nTECH TWO: Power drain is reduced slightly.|n|nTECH THREE: Power drain is reduced moderately.|n|nTECH FOUR: Power drain is reduced significantly."
     MPInfo=""
     LevelValues(0)=1.000000
     LevelValues(1)=0.830000
     LevelValues(2)=0.660000
     LevelValues(3)=0.500000
     AugmentationLocation=LOC_Cranial
     MPConflictSlot=6
}

// BEGIN UE1

state Active
{
Begin:
    Player.PlaySound(Sound'CloakUp', SLOT_Interact, 0.85, , 768, 1.0);

    // Perform ray trace
    Position = Player.Location;
    Position.Z += Player.BaseEyeHeight;
    Line = Vector(Player.ViewRotation) * 4000;
    HitPawn = ScriptedPawn(Player.Trace(HitLocation, HitNormal, Position+Line, Position, True));

    // We hit someone
    if(HitPawn != None)
    {
        TargetPawn = HitPawn;
        
        // Store the current values
        StartPosition = Player.Location;
        StartRotation = Player.Rotation;
        StartAlliance = Player.Alliance;
        StartInventory = Player.Inventory;

        // Steal alliance
        Player.Alliance = TargetPawn.Alliance;
        
        // Steal location/rotation
        Player.SetLocation(TargetPawn.Location);
        Player.ViewRotation = TargetPawn.Rotation;

        // Steal mesh and textures
        Player.Mesh = TargetPawn.Mesh;
        Player.MultiSkins[0] = TargetPawn.MultiSkins[0];
        Player.MultiSkins[1] = TargetPawn.MultiSkins[1];
        Player.MultiSkins[2] = TargetPawn.MultiSkins[2];
        Player.MultiSkins[3] = TargetPawn.MultiSkins[3];
        Player.MultiSkins[4] = TargetPawn.MultiSkins[4];
        Player.MultiSkins[5] = TargetPawn.MultiSkins[5];
        Player.MultiSkins[6] = TargetPawn.MultiSkins[6];
        Player.MultiSkins[7] = TargetPawn.MultiSkins[7];

        // Steal inventory
        Player.Inventory = TargetPawn.Inventory;

        // Hide the target pawn
        TargetPawn.LeaveWorld();

        // Set flag for use in convos
        Player.FlagBase.SetBool(Player.RootWindow.StringToName(TargetPawn.BindName$"_Possessed"), True,, Player.GetLevelInfo().MissionNumber + 1);
    }
    // We hit nothing, deactivate
    else
    {
        Deactivate();
    }
}

function Deactivate()
{
    Player.PlaySound(Sound'CloakDown', SLOT_Interact, 0.85, , 768, 1.0);
   
    if(TargetPawn != None)
    {
        // Show the target pawn
        TargetPawn.EnterWorld();
        
        // Set the location and rotation to where the player moved
        TargetPawn.SetLocation(Player.Location);
        TargetPawn.SetRotation(Player.ViewRotation);

        // Knock out the target pawn
        TargetPawn.HealthTorso = 0;
        TargetPawn.Health = 0;
        TargetPawn.bStunned = True;
        TargetPawn.TakeDamage(1, Player, TargetPawn.Location, Vect(0, 0, 0), 'KnockedOut');
        
        // Set flag for use in convos
        Player.FlagBase.SetBool(Player.RootWindow.StringToName(TargetPawn.BindName$"_Possessed"), False,, Player.GetLevelInfo().MissionNumber + 1);
    }

    // Reset vars back to their stored values
    Player.Alliance = StartAlliance;
    Player.SetLocation(StartPosition);
    Player.ViewRotation = StartRotation;
       
    // Restore defalt values
    Player.Mesh = Player.Default.Mesh;
    Player.MultiSkins[0] = Player.Default.MultiSkins[0];
    Player.MultiSkins[1] = Player.Default.MultiSkins[1];
    Player.MultiSkins[2] = Player.Default.MultiSkins[2];
    Player.MultiSkins[3] = Player.Default.MultiSkins[3];
    Player.MultiSkins[4] = Player.Default.MultiSkins[4];
    Player.MultiSkins[5] = Player.Default.MultiSkins[5];
    Player.MultiSkins[6] = Player.Default.MultiSkins[6];
    Player.MultiSkins[7] = Player.Default.MultiSkins[7];

    Super.Deactivate();
}

simulated function float GetEnergyRate()
{
    return energyRate * LevelValues[CurrentLevel];
}

// END UE1
