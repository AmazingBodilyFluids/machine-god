class ElCoyote extends HumanMilitary;

defaultproperties
{
     BindName="ElCoyote"
     FamiliarName="El Coyote"
     UnfamiliarName="Big guy"
     CarcassType=Class'MachineGod.ElCoyoteCarcass'
     WalkingSpeed=0.300000
     bImportant=True
     bInvincible=False
     BaseAssHeight=-23.000000
     walkAnimMult=1.050000
     GroundSpeed=200.000000
     Mesh=LodMesh'DeusExCharacters.GM_DressShirt_B'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.ChadTex1'
     MultiSkins(1)=Texture'DeusExCharacters.Skins.JunkieFemaleTex2'
     MultiSkins(2)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(3)=Texture'DeusExCharacters.Skins.BartenderTex0'
     MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(5)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(6)=Texture'DeusExItems.Skins.BlackMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
     CollisionRadius=20.000000
     CollisionHeight=47.500000
}
