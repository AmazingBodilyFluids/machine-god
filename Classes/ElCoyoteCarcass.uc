class ElCoyoteCarcass extends DeusExCarcass;

defaultproperties
{
     Mesh=LodMesh'DeusExCharacters.GM_DressShirt_B_Carcass'
     Mesh2=LodMesh'DeusExCharacters.GM_DressShirt_B_CarcassB'
     Mesh3=LodMesh'DeusExCharacters.GM_DressShirt_B_CarcassC'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.ChadTex1'
     MultiSkins(1)=Texture'DeusExCharacters.Skins.JunkieFemaleTex2'
     MultiSkins(2)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(3)=Texture'DeusExCharacters.Skins.BartenderTex0'
     MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(5)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(6)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.PinkMaskTex'
     CollisionRadius=40.000000
}
