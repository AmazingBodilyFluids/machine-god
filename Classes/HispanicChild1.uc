class HispanicChild1 extends HumanCivilian;

defaultproperties
{
     BindName="HispanicChild1"
     FamiliarName=""
     UnfamiliarName="Child"
     CarcassType=Class'MachineGod.HispanicChild1Carcass'
     WalkingSpeed=0.300000
     bImportant=False
     bInvincible=False
     BaseAssHeight=-23.000000
     walkAnimMult=1.050000
     GroundSpeed=200.000000
     Mesh=LodMesh'DeusExCharacters.GMK_DressShirt'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.AlexJacobsonTex0'
     MultiSkins(1)=Texture'DeusExCharacters.Skins.LowerClassMaleTex1'
     MultiSkins(2)=Texture'DeusExCharacters.Skins.PantsTex1'
     MultiSkins(3)=Texture'DeusExCharacters.Skins.ChildMaleTex0'
     MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(5)=Texture'DeusExCharacters.Skins.ChildMaleTex1'
     MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
     CollisionRadius=20.000000
     CollisionHeight=32.000000
}
