class HispanicChild2Carcass extends DeusExCarcass;

defaultproperties
{
     Mesh=LodMesh'DeusExCharacters.GMK_DressShirt_Carcass'
     Mesh2=LodMesh'DeusExCharacters.GMK_DressShirt_CarcassB'
     Mesh3=LodMesh'DeusExCharacters.GMK_DressShirt_CarcassC'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.MiscTex1'
     MultiSkins(1)=Texture'DeusExCharacters.Skins.ChildMaleTex1'
     MultiSkins(2)=Texture'DeusExCharacters.Skins.PantsTex3'
     MultiSkins(3)=Texture'DeusExCharacters.Skins.ChildMaleTex0'
     MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(5)=Texture'DeusExCharacters.Skins.ChildMaleTex1'
     MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
     CollisionRadius=40.000000
}
