class HispanicFemale1 extends HumanCivilian;

defaultproperties
{
     BindName="HispanicFemale1"
     FamiliarName=""
     UnfamiliarName="Female civilian"
     CarcassType=Class'MachineGod.HispanicFemale1Carcass'
     WalkingSpeed=0.300000
     bImportant=False
     bInvincible=False
     bIsFemale=True
     BaseAssHeight=-23.000000
     walkAnimMult=1.050000
     GroundSpeed=200.000000
     Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.LowerClassFemaleTex0'
     MultiSkins(1)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(2)=Texture'DeusExCharacters.Skins.LowerClassFemaleTex0'
     MultiSkins(3)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(4)=Texture'DeusExItems.Skins.BlackMaskTex'
     MultiSkins(5)=Texture'DeusExCharacters.Skins.Female1Tex0'
     MultiSkins(6)=Texture'DeusExCharacters.Skins.PantsTex2'
     MultiSkins(7)=Texture'DeusExCharacters.Skins.LowerClassFemaleTex1'
     CollisionRadius=20.000000
     CollisionHeight=42.000000
}
