class HispanicMale1 extends HumanCivilian;

defaultproperties
{
     BindName="HispanicMale1"
     FamiliarName=""
     UnfamiliarName="Male civilian"
     CarcassType=Class'MachineGod.HispanicMale1Carcass'
     WalkingSpeed=0.300000
     bImportant=False
     bInvincible=False
     BaseAssHeight=-23.000000
     walkAnimMult=1.050000
     GroundSpeed=200.000000
     Mesh=LodMesh'DeusExCharacters.GM_DressShirt'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.BoatPersonTex0'
     MultiSkins(1)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(2)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(3)=Texture'DeusExCharacters.Skins.JunkieFemaleTex2'
     MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(5)=Texture'DeusExCharacters.Skins.BoatPersonTex1'
     MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
     CollisionRadius=20.000000
     CollisionHeight=47.500000
}
