class HispanicPoliceFemale1Carcass extends DeusExCarcass;

defaultproperties
{
     Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants_Carcass'
     Mesh2=LodMesh'DeusExCharacters.GFM_TShirtPants_CarcassB'
     Mesh3=LodMesh'DeusExCharacters.GFM_TShirtPants_CarcassC'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.Businesswoman1Tex0'
     MultiSkins(1)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(2)=Texture'DeusExCharacters.Skins.Businesswoman1Tex0'
     MultiSkins(3)=Texture'DeusExCharacters.Skins.FramesTex2'
     MultiSkins(4)=Texture'DeusExCharacters.Skins.LensesTex2'
     MultiSkins(5)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(6)=Texture'DeusExCharacters.Skins.HKMilitaryTex2'
     MultiSkins(7)=Texture'DeusExCharacters.Skins.HKMilitaryTex1'
     CollisionRadius=40.000000
}
