class HispanicThug2 extends HumanThug;

defaultproperties
{
     BindName="HispanicThug2"
     FamiliarName=""
     UnfamiliarName="Thug"
     CarcassType=Class'MachineGod.HispanicThug2Carcass'
     WalkingSpeed=0.300000
     bImportant=False
     bInvincible=False
     BaseAssHeight=-23.000000
     walkAnimMult=1.050000
     GroundSpeed=200.000000
     Mesh=LodMesh'DeusExCharacters.GM_Trench_F'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.SkinTex2'
     MultiSkins(1)=Texture'DeusExCharacters.Skins.BumMale2Tex2'
     MultiSkins(2)=Texture'DeusExCharacters.Skins.JockTex3'
     MultiSkins(3)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(4)=Texture'DeusExCharacters.Skins.FordSchickTex1'
     MultiSkins(5)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
     CollisionRadius=20.000000
     CollisionHeight=47.500000
}
