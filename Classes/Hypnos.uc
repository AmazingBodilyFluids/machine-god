class Hypnos extends Pigeon;

defaultproperties
{
    BindName="Hypnos"
    FamiliarName="Hypnos"
    UnfamiliarName="Hypnos"
    MultiSkins(0)=FireTexture'Effects.Electricity.Nano_SFX'
    AirSpeed=300.000000
    bInvincible=True
    bHighlight=False
    bReactPresence=False
    bUnlit=True
    Style=STY_Translucent
    Orders="Standing"
    CollisionHeight=20
    DrawScale=4.0
}
