class Image_17_Evidence expands DataVaultImage;

defaultproperties
{
     imageTextures(0)=Texture'MachineGod.DataVaultImages.Image_17_Evidence_1'
     imageTextures(1)=Texture'MachineGod.DataVaultImages.Image_17_Evidence_2'
     imageTextures(2)=Texture'MachineGod.DataVaultImages.Image_17_Evidence_3'
     imageTextures(3)=Texture'MachineGod.DataVaultImages.Image_17_Evidence_4'
     imageDescription="Evidence against officer Luis"
     colNoteTextNormal=(R=50,G=50)
     colNoteTextFocus=(R=0,G=0)
     colNoteBackground=(R=32,G=32)
}
