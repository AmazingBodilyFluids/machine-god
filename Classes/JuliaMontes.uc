class JuliaMontes extends HumanCivilian;

defaultproperties
{
     BaseAccuracy=-0.150000
     BaseAssHeight=-23.000000
     BindName="JuliaMontes"
     CarcassType=Class'MachineGod.JuliaMontesCarcass'
     CollisionHeight=42.000000
     CollisionRadius=20.000000
     FamiliarName="Julia Montes"
     GroundSpeed=200.000000
     Mesh=LodMesh'DeusExCharacters.GFM_Trench'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.LowerClassFemaleTex0'
     MultiSkins(1)=Texture'DeusExCharacters.Skins.JCDentonTex2'
     MultiSkins(2)=Texture'DeusExCharacters.Skins.PantsTex9'
     MultiSkins(3)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(4)=Texture'DeusExCharacters.Skins.ThugMaleTex1'
     MultiSkins(5)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
     UnfamiliarName="Female civilian"
     WalkingSpeed=0.300000
     bAimForHead=True
     bAvoidAim=True
     bCanStrafe=True
     bFearAlarm=False
     bFearCarcass=False
     bFearDistress=False
     bFearHacking=False
     bFearIndirectInjury=False
     bFearInjury=False
     bFearProjectiles=False
     bFearShot=False
     bFearWeapon=False
     bImportant=True
     bInvincible=True
     bIsFemale=True
     bUseFallbackWeapons=True
     bMustFaceTarget=False
     walkAnimMult=1.050000
}
