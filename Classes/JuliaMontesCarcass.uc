class JuliaMontesCarcass extends DeusExCarcass;

defaultproperties
{
     Mesh=LodMesh'DeusExCharacters.GFM_Trench_Carcass'
     Mesh2=LodMesh'DeusExCharacters.GFM_Trench_CarcassB'
     Mesh3=LodMesh'DeusExCharacters.GFM_Trench_CarcassC'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.LowerClassFemaleTex0'
     MultiSkins(1)=Texture'DeusExCharacters.Skins.JCDentonTex2'
     MultiSkins(2)=Texture'DeusExCharacters.Skins.PantsTex9'
     MultiSkins(3)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(4)=Texture'DeusExCharacters.Skins.ThugMaleTex1'
     MultiSkins(5)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
     CollisionRadius=40.000000
}
