class LarryWinger extends HumanCivilian;

defaultproperties
{
     BindName="LarryWinger"
     FamiliarName="Larry Winger"
     UnfamiliarName="HR director"
     CarcassType=Class'MachineGod.LarryWingerCarcass'
     bImportant=True
     Mesh=LodMesh'DeusExCharacters.GM_Trench_F'
     MultiSkins(0)=Texture'MachineGod.Tijuana_Streets.Characters.LarryWingerTex0'
     MultiSkins(1)=Texture'DeusExCharacters.Skins.JosephManderleyTex2'
     MultiSkins(2)=Texture'DeusExCharacters.Skins.Male2Tex2'
     MultiSkins(3)=Texture'DeusExItems.Skins.BlackMaskTex'
     MultiSkins(4)=Texture'MachineGod.Tijuana_Streets.Characters.LarryWingerTex1'
     MultiSkins(5)=Texture'DeusExCharacters.Skins.JosephManderleyTex2'
     MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
     CollisionRadius=20.000000
     CollisionHeight=47.500000
}
