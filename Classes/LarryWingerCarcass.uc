class LarryWingerCarcass extends DeusExCarcass;

defaultproperties
{
     Mesh=LodMesh'DeusExCharacters.GM_Trench_F_Carcass'
     Mesh2=LodMesh'DeusExCharacters.GM_Trench_F_CarcassB'
     Mesh3=LodMesh'DeusExCharacters.GM_Trench_F_CarcassC'
     MultiSkins(0)=Texture'MachineGod.Tijuana_Streets.Characters.LarryWingerTex0'
     MultiSkins(1)=Texture'DeusExCharacters.Skins.JosephManderleyTex2'
     MultiSkins(2)=Texture'DeusExCharacters.Skins.Male2Tex2'
     MultiSkins(3)=Texture'DeusExItems.Skins.BlackMaskTex'
     MultiSkins(4)=Texture'MachineGod.Tijuana_Streets.Characters.LarryWingerTex1'
     MultiSkins(5)=Texture'DeusExCharacters.Skins.JosephManderleyTex2'
     MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
     CollisionRadius=40.000000
}
