class Mike extends HumanMilitary;

defaultproperties
{
     BindName="Mike"
     FamiliarName="Mike"
     UnfamiliarName="Mike"
     CarcassType=Class'MachineGod.MikeCarcass'
     WalkingSpeed=0.300000
     bImportant=True
     bInvincible=True
     BaseAssHeight=-23.000000
     walkAnimMult=1.050000
     GroundSpeed=200.000000
     Mesh=LodMesh'DeusExCharacters.GM_Trench'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.ThugMale3Tex0'
     MultiSkins(1)=Texture'DeusExCharacters.Skins.JockTex2'
     MultiSkins(2)=Texture'DeusExCharacters.Skins.Male2Tex2'
     MultiSkins(3)=Texture'DeusExCharacters.Skins.Male2Tex2'
     MultiSkins(4)=Texture'DeusExCharacters.Skins.TriadRedArrowTex1'
     MultiSkins(5)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(6)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.PinkMaskTex'
     CollisionRadius=20.000000
     CollisionHeight=47.500000
}
