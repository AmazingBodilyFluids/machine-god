class MikeCarcass extends DeusExCarcass;

defaultproperties
{
     Mesh=LodMesh'DeusExCharacters.GM_Trench_Carcass'
     Mesh2=LodMesh'DeusExCharacters.GM_Trench_CarcassB'
     Mesh3=LodMesh'DeusExCharacters.GM_Trench_CarcassC'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.ThugMale3Tex0'
     MultiSkins(1)=Texture'DeusExCharacters.Skins.JockTex2'
     MultiSkins(2)=Texture'DeusExCharacters.Skins.Male2Tex2'
     MultiSkins(3)=Texture'DeusExCharacters.Skins.Male2Tex2'
     MultiSkins(4)=Texture'DeusExCharacters.Skins.TriadRedArrowTex1'
     MultiSkins(5)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(6)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.PinkMaskTex'
     CollisionRadius=40.000000
}
