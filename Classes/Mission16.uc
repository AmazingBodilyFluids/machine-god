class Mission16 expands MissionScript;

// BEGIN UE1

function InitStateMachine()
{
    Super.InitStateMachine();
    
    FirstFrame();
}

function FirstFrame()
{
    Super.FirstFrame();

    // Hide JC under the holocom
    if ( LocalURL == "16_VANDENBERG_COMPUTER" )
    {
        Flags.SetBool( '16_Hide_JC', True,, 0 );
    }
}

function Timer()
{
    local ScriptedPawn NPC;
    local Vector JCDentonLocation;

    Super.Timer();

    if ( LocalURL == "16_VANDENBERG_COMPUTER" )
    {
        JCDentonLocation.X = 1080;
        JCDentonLocation.Y = 2792;
       
        // If we set the hide JC flag from the convo, move him under the holocom
        if ( Flags.GetBool('16_Hide_JC') && !Flags.GetBool('16_JC_Hidden') )
        {
            JCDentonLocation.Z = -2280;

            foreach AllActors(class'ScriptedPawn', NPC, 'JCDentonHelios')
            {
                NPC.SetLocation(JCDentonLocation);
            }

            Flags.SetBool( '16_JC_Hidden', True,, 1 );
        }
       
        // If we unset the hide JC flag from the convo, move him on top of the holocom
        if ( !Flags.GetBool('16_Hide_JC') && Flags.GetBool('16_JC_Hidden') )
        {
            JCDentonLocation.Z = -2130;
            
            foreach AllActors(class'ScriptedPawn', NPC, 'JCDentonHelios')
            {
                NPC.SetLocation(JCDentonLocation);
            }

            Flags.SetBool( '16_JC_Hidden', False,, 1 );
        }
    }
}

// END UE1
