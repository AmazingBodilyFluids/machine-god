//=============================================================================
// Mission17 - Tijuana, Mexico
//=============================================================================
class Mission17 expands MissionScript;

// BEGIN UE1

function InitStateMachine()
{
    Super.InitStateMachine();
    
    FirstFrame();
}

function FirstFrame()
{
    Super.FirstFrame();
}

function Timer()
{
    Super.Timer();

    if (LocalURL == "17_TIJUANA_SLUMS")
    {
        CheckOfficerLuis();
        CheckPoliceHarassingJuliaMontes();
    }
}

// -----------------------------------------------------
// Check on the officer Luis quest
// -----------------------------------------------------
function CheckOfficerLuis()
{
    local ComputerPersonal C;
    local ScriptedPawn P;

    // Add sending evidence option to computers if the player found the image
    if(Flags.GetBool('Player_Picked_Up_Evidence'))
    {
        foreach AllActors(class'ComputerPersonal', C, 'BorderComputers')
        {
            C.SpecialOptions[0].bTriggerOnceOnly = True;
            C.SpecialOptions[0].Text = "Send evidence to sergeant";
            C.SpecialOptions[0].TriggerEvent = 'UploadLuisEvidence';
        }
    }
    
    // Set an extra convenience flag
    if(Flags.GetBool('Player_Sent_Evidence') || Flags.GetBool('Luis_Dead'))
    {
        Flags.SetBool('Job_Resolved', True,, 18);
    }

    // Make Luis drop his weapon
    if(Flags.GetBool('Luis_Should_Drop_Weapon') && !Flags.GetBool('Luis_Dropped_Weapon'))
    {
        foreach AllActors(class'ScriptedPawn', P, 'Luis')
        {
            P.bKeepWeaponDrawn = False;
            P.DropWeapon();
        }
        
        Flags.SetBool('Luis_Dropped_Weapon', True,, 18);
    }
}

// -----------------------------------------------------
// Determine what to make the police do with Julia Montes
// -----------------------------------------------------
function CheckPoliceHarassingJuliaMontes()
{
    local Trigger T;
    local DeusExCarcass C;
    local ScriptedPawn P;
    local int DeadPoliceCount;
        
    // Check if the police are dead
    DeadPoliceCount = 0;

    foreach AllActors(class'DeusExCarcass', C, 'PoliceHarassingJuliaMontes')
    {
        DeadPoliceCount++;

        // Did the player kill the police?
        if(C.KillerBindName == "JCDenton")
        {
            Flags.SetBool('Player_Killed_PoliceHarassingJuliaMontes', True,, 18); 
        }
    }

    if(DeadPoliceCount > 1)
    {
        Flags.SetBool('PoliceHarassingJuliaMontes_Resolved', True,, 18);
        return;
    }
        
    // If it's already been determined, cancel the check
    if(Flags.GetBool('PoliceHarassingJuliaMontes_Resolved'))
    {
        return;
    }
   
    // The conversation has not yet played, and nobody died, cancel
    if(!Flags.GetBool('OverhearJuliaPolice_Played') && DeadPoliceCount == 0)
    {
        return;
    }
    
    // Police leave JuliaMontes alone
    if(Flags.GetBool('Player_Convinced_PoliceHarassingJuliaMontes'))
    {
        foreach AllActors(class'Trigger', T, 'PoliceLeaveJuliaMontes')
        {
            T.Trigger(Self, Player);
        }

        Flags.SetBool('Police_Left_JuliaMontes', True,, 18);
        Flags.SetBool('PoliceHarassingJuliaMontes_Resolved', True,, 18);
        return;
    }
    
    // The player is busy, cancel
    if(Player.InConversation())
    {
        return;
    }
    
    // Police attack JuliaMontes 
    foreach AllActors(class'Trigger', T, 'PoliceAttackJuliaMontes')
    {
        T.Trigger(Self, Player);
    }

    Flags.SetBool('Police_Attacked_JuliaMontes', True,, 18);
}

// END UE1
