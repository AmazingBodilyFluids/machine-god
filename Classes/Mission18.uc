//=============================================================================
// Mission18 - Tijuana, Mexico
//=============================================================================
class Mission18 expands MissionScript;

// BEGIN UE1

var float RebootTimer;
var float RebootSpriteFlash;
var Texture RebootSprite;
var ScriptedPawn aScriptedPawn;
var PlayerStart aPlayerStart;
var DeusExMover aDeusExMover;
var PatrolPoint aPatrolPoint;
var Actor aActor;

function InitStateMachine()
{
    Super.InitStateMachine();
    
    FirstFrame();
}

function FirstFrame()
{
    Super.FirstFrame();

    RebootSprite = Texture(DynamicLoadObject("Effects.Wepn_Prod_FX", class'FireTexture'));

    foreach AllActors(class'Actor', aActor, 'DevPlayerStart')
    {
        Player.SetLocation(aActor.Location);
        Player.SetRotation(aActor.Rotation);
    }

    // Open the train doors
    foreach AllActors(class'DeusExMover', aDeusExMover, 'TrainDoors')
    {
        aDeusExMover.DoOpen();
    }
}

function Timer()
{
    local ScriptedPawn aScriptedPawn;
    local Cat aCat;
    
    Super.Timer();

    // Go to reboot state if needed
    if (!IsInState('Rebooting') && Flags.GetBool('Rebooting'))
    {
        GoToState('Rebooting');
    }

    // Manage Mike
    if (Flags.GetBool('Mike_Following_Player'))
    {
        foreach AllActors(class'ScriptedPawn', aScriptedPawn, 'Mike')
        {
            // Make sure Mike is in following state
            if(!Player.InConversation() && !aScriptedPawn.IsInState('Following'))
            {
                aScriptedPawn.GoToState('Following');
                aScriptedPawn.Orders = 'Following';
            }

            // Make sure Mike is not left behind
            if(VSize(aScriptedPawn.Location - Player.Location) > 2048)
            {
                aScriptedPawn.SetLocation(Player.Location - Normal(Vector(Player.Rotation)) * (Player.CollisionRadius * 10));
            }
        }
    }

    // Hide Larry Winger, if we haven't spoken to Hector yet
    foreach AllActors(class'ScriptedPawn', aScriptedPawn, 'LarryWinger')
    {
        if (Flags.GetBool('AskHectorAboutClara_Played') && !aScriptedPawn.bInWorld)
        {
            aScriptedPawn.EnterWorld();
        }
        else if (!Flags.GetBool('AskHectorAboutClara_Played') && aScriptedPawn.bInWorld)
        {
            aScriptedPawn.LeaveWorld();
        }
    }

    // Set flag if player has liquor (for convenience during convos)
    Flags.SetBool('Player_Has_Liquor', Player.FindInventoryType(class'LiquorBottle') != None);

    // Update visible cats
    foreach AllActors(class'ScriptedPawn', aScriptedPawn)
    {
        foreach AllActors(class'Cat', aCat, Player.RootWindow.StringToName("Cat" $ aScriptedPawn.Tag))
        {
            aCat.PutInWorld(Flags.GetBool(Player.RootWindow.StringToName(aScriptedPawn.Tag $ "_Woke")));
        }
    }    
}

// -----------------------------------------------------
// Rebooting
// -----------------------------------------------------
state Rebooting
{
Begin:
    Sleep(0.5);
    
    Player.PlaySound(sound'Airplane3',,,,,2.0);
    RebootSpriteFlash = 0.001 + (0.1 - 0.001) * FRand();

    foreach AllActors(class'ScriptedPawn', aScriptedPawn, 'Mike')
    {
        aScriptedPawn.PlayCowering();
    }
    
    for (RebootTimer = 3.0; RebootTimer > 0; RebootTimer -= RebootSpriteFlash)
    {
        Player.ShakeView(RebootSpriteFlash, 128, (3.0 - RebootTimer) + 4 + Rand(12));
        Sleep(RebootSpriteFlash);
        
        if (Player.Sprite == None)
        {
            Player.Sprite = RebootSprite;
        }
        else
        {
            Player.Sprite = None;
        }
        
        RebootSpriteFlash = 0.001 + ((RebootTimer / 2) - 0.001) * FRand();
    }

    Flags.SetBool('Rebooting', False,, 19);
    Player.Sprite = None;
        
    // Send player to the train
    foreach AllActors(class'PlayerStart', aPlayerStart)
    {
        Player.SetLocation(aPlayerStart.Location); 
        Player.SetRotation(aPlayerStart.Rotation); 
    }
    
    // Move Mike to the train and force his convo
    foreach AllActors(class'ScriptedPawn', aScriptedPawn, 'Mike')
    {
        foreach AllActors(class'PatrolPoint', aPatrolPoint, Player.RootWindow.StringToName(aScriptedPawn.Tag $ "WokePosition"))
        {
            aScriptedPawn.SetLocation(aPatrolPoint.Location);
            aScriptedPawn.SetRotation(aPatrolPoint.Rotation);
            aScriptedPawn.DesiredRotation = aPatrolPoint.Rotation;
        }
        
        aScriptedPawn.Frob(Player, Player.Inventory); 
    }
    
    GoToState('');
}

// END UE1
