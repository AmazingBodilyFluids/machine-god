class Splash expands MissionScript;

// BEGIN UE1

function InitStateMachine()
{
    Super.InitStateMachine();
    
    FirstFrame();
}

function FirstFrame()
{
    local InterpolateTrigger T;

    Super.FirstFrame();

    foreach AllActors(class'InterpolateTrigger', T)
    {
        T.Trigger(Player, Player);
    }
}

// END UE1
