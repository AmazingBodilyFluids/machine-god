class TextImport expands Object abstract;

#exec DEUSEXTEXT IMPORT FILE=Text\16_GeneralsQuarters_AugCanDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\16_GeneralsQuarters_SecurityConsoleDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\16_Hallway_StackedBoxesDataCube.txt

#exec DEUSEXTEXT IMPORT FILE=Text\17_Border_ComputerAccessDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\17_Border_EvidenceDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\17_Slums_BorderDoorCodeDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\17_Email01.txt
#exec DEUSEXTEXT IMPORT FILE=Text\17_Email02.txt
#exec DEUSEXTEXT IMPORT FILE=Text\17_EmailMenu_anon123.txt

#exec DEUSEXTEXT IMPORT FILE=Text\18_ClaraTranscriptDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_StaffIndexDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_Email01.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_EmailMenu_lwinger.txt
