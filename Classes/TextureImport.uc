class TextureImport expands Object abstract; 

#exec TEXTURE IMPORT NAME=Logo FILE=Textures\UI\Logo.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=Title_1 FILE=Textures\UI\Title_1.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=Title_2 FILE=Textures\UI\Title_2.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=AugIconPossess FILE=Textures\UI\AugIconPossess.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=AugIconPossess_Small FILE=Textures\UI\AugIconPossess_Small.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=FakeLoading FILE=Textures\UI\FakeLoading.pcx GROUP=UI MIPS=Off FLAGS=2

#exec TEXTURE IMPORT NAME=Image_17_Evidence_1 FILE=Textures\DataVaultImages\Image_17_Evidence_1.pcx GROUP=DataVaultImages MIPS=Off
#exec TEXTURE IMPORT NAME=Image_17_Evidence_2 FILE=Textures\DataVaultImages\Image_17_Evidence_2.pcx GROUP=DataVaultImages MIPS=Off
#exec TEXTURE IMPORT NAME=Image_17_Evidence_3 FILE=Textures\DataVaultImages\Image_17_Evidence_3.pcx GROUP=DataVaultImages MIPS=Off
#exec TEXTURE IMPORT NAME=Image_17_Evidence_4 FILE=Textures\DataVaultImages\Image_17_Evidence_4.pcx GROUP=DataVaultImages MIPS=Off

#exec TEXTURE IMPORT NAME=Graffiti01_1 FILE=Textures\Tijuana_Slums\Deco\Graffiti01_1.pcx GROUP=Decorations FLAGS=2
#exec TEXTURE IMPORT NAME=Graffiti01_2 FILE=Textures\Tijuana_Slums\Deco\Graffiti01_2.pcx GROUP=Decorations FLAGS=2
#exec TEXTURE IMPORT NAME=Graffiti02 FILE=Textures\Tijuana_Slums\Deco\Graffiti02.pcx GROUP=Decorations FLAGS=2
#exec TEXTURE IMPORT NAME=Graffiti03 FILE=Textures\Tijuana_Slums\Deco\Graffiti03.pcx GROUP=Decorations FLAGS=2
