class TiffanySavagePlayer extends JCDentonMale;

var Actor aActor;

defaultproperties
{
    BindName="JCDenton"
    TruePlayerName="Tiffany Savage"
    FamiliarName="Tiffany Savage"
    UnfamiliarName="Tiffany Savage"
    CarcassType=Class'DeusEx.TiffanySavageCarcass'
    bIsFemale=True
    BaseEyeHeight=38.000000
    Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants'
    MultiSkins(0)=Texture'DeusExCharacters.Skins.TiffanySavageTex0'
    MultiSkins(1)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(2)=Texture'DeusExCharacters.Skins.TiffanySavageTex0'
    MultiSkins(3)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(4)=Texture'DeusExItems.Skins.BlackMaskTex'
    MultiSkins(5)=Texture'DeusExCharacters.Skins.TiffanySavageTex0'
    MultiSkins(6)=Texture'DeusExCharacters.Skins.TiffanySavageTex2'
    MultiSkins(7)=Texture'DeusExCharacters.Skins.TiffanySavageTex1'
    CollisionRadius=20.000000
    CollisionHeight=43.000000
    JumpSound=Sound'MachineGod.Player.Jump'
    HitSound1=Sound'MachineGod.Player.PainSmall'
    HitSound2=Sound'MachineGod.Player.PainMedium'
    Land=Sound'MachineGod.Player.Land'
    Die=Sound'MachineGod.Player.Death'
}

// BEGIN UE1 

event TravelPostAccept()
{
    Super.TravelPostAccept(); 
    MultiSkins[0] = Texture'DeusExCharacters.Skins.TiffanySavageTex0';
    MultiSkins[3] = Texture'DeusExItems.Skins.GrayMaskTex';

    foreach AllActors(class'Actor', aActor, 'StartIntroSequence')
    {
        Ghost();
        GoToState('Interpolating');    
        Event = 'PlayerIntroPath';
        Tag = 'Player';
        aActor.Trigger(Self, Self);
        break;
    }
}


// ----------------------------------------
// Console commands
// ----------------------------------------
exec function Teleport(Name TagName)
{
    foreach AllActors(class'Actor', aActor, TagName)
    {
        SetLocation(aActor.Location);
        SetRotation(aActor.Rotation);
    }
}

exec function Flag(Name FlagName, bool bValue)
{
    FlagBase.SetBool(FlagName, bValue);
}

exec function Sonic(bool bValue)
{
    if(!bValue)
    {
        ConsoleCommand("set tiffanysavageplayer groundspeed 320");
    }
    else
    {
        ConsoleCommand("set tiffanysavageplayer groundspeed 1000");
    }
}

// ----------------------------------------
// Hijack this method to load our first map
// ----------------------------------------
function ShowIntro(optional bool bStartNewGame)
{
    if (DeusExRootWindow(rootWindow) != None)
        DeusExRootWindow(rootWindow).ClearWindowStack();

    bStartNewGameAfterIntro = bStartNewGame;

    // Make sure all augmentations are OFF before going into the intro
    AugmentationSystem.DeactivateAll();

    // Reset the player
    Level.Game.SendPlayer(Self, "16_Vandenberg_Barracks");
}

// ---------------------------------------------
// Override the vanilla aug manager with our own
// ---------------------------------------------
function InitializeSubSystems()
{
    // Spawn the BarkManager
    if (BarkManager == None)
        BarkManager = Spawn(class'BarkManager', Self);

    // Spawn the Color Manager
    CreateColorThemeManager();
    ThemeManager.SetOwner(self);
    
    // Install the augmentation system if not found
    if (TiffanySavageAugmentationManager(AugmentationSystem) == None)
    {
        AugmentationSystem = Spawn(class'TiffanySavageAugmentationManager', Self);
        AugmentationSystem.CreateAugmentations(Self);
        AugmentationSystem.AddDefaultAugmentations();
        AugmentationSystem.SetOwner(Self);       
    }
    else
    {
        AugmentationSystem.SetPlayer(Self);
        AugmentationSystem.SetOwner(Self);
    }
    
    // Install the skill system if not found
    if (SkillSystem == None)
    {
        SkillSystem = Spawn(class'SkillManager', Self);
        SkillSystem.CreateSkills(Self);
    }
    else
    {
        SkillSystem.SetPlayer(Self);
    }
    
    if ((Level.Netmode == NM_Standalone) || (!bBeltIsMPInventory))
    {
        // Give the player a keyring
        CreateKeyRing();
    }
}

// ----------------------------
// Fix player sounds for damage
// ----------------------------
function PlayTakeHitSound(int Damage, name damageType, int Mult)
{
    local float rnd;

    if ( Level.TimeSeconds - LastPainSound < FRand() + 0.5)
        return;

    LastPainSound = Level.TimeSeconds;

    if (Region.Zone.bWaterZone)
    {
        if (damageType == 'Drowned')
        {
            if (FRand() < 0.8)
                PlaySound(sound'MachineGod.Player.Drown', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        }
        else
            PlaySound(sound'MachineGod.Player.PainSmall', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
    }
    else
    {
        // Body hit sound for multiplayer only
        if (((damageType=='Shot') || (damageType=='AutoShot'))  && ( Level.NetMode != NM_Standalone ))
        {
            PlaySound(sound'BodyHit', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        }

        if ((damageType == 'TearGas') || (damageType == 'HalonGas'))
            PlaySound(sound'MachineGod.Player.EyePain', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        else if (damageType == 'PoisonGas')
            PlaySound(sound'MachineGod.Player.Cough', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        else
        {
            rnd = FRand();
            if (rnd < 0.33)
                PlaySound(sound'MachineGod.Player.PainSmall', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
            else if (rnd < 0.66)
                PlaySound(sound'MachineGod.Player.PainMedium', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
            else
                PlaySound(sound'MachineGod.Player.PainLarge', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        }
        AISendEvent('LoudNoise', EAITYPE_Audio, FMax(Mult * TransientSoundVolume, Mult * 2.0));
    }
}

// ----------------------------
// Fix player sounds for gasping
// ----------------------------
function Gasp()
{
    PlaySound(sound'MachineGod.Player.Gasp', SLOT_Pain,,,, RandomPitch());
}

// ----------------------------
// Fix player sounds for dying
// ----------------------------
function PlayDyingSound()
{
    if (Region.Zone.bWaterZone)
        PlaySound(sound'MachineGod.Player.WaterDeath', SLOT_Pain,,,, RandomPitch());
    else
        PlaySound(sound'MachineGod.Player.Death', SLOT_Pain,,,, RandomPitch());
}

// ----------------------------------------------------------------------
// Hijack this method to ensure pawns are present 
// ----------------------------------------------------------------------
function bool StartConversation(
    Actor InvokeActor, 
    EInvokeMethod InvokeMethod, 
    optional Conversation Con,
    optional bool bAvoidState,
    optional bool bForcePlay
    )
{
    local ConEvent Event;
    local ConEventSpeech EventSpeech;
    local ScriptedPawn aScriptedPawn;
    local Rotator HelpRotator;
    local int Radius;

    // Only try this trick if the player frobbed an NPC
    if(InvokeMethod == IM_Frob)
    {
        if(Con == None)
        {
            Con = GetActiveConversation(InvokeActor, InvokeMethod);
        }
        
        // Set maximum radius and initialize the rotator
        Radius = 128;
        HelpRotator = Rotation;

        // Loop all SpeechEvents in the convo to find their actors
        Event = Con.EventList;

        while(Event != None)
        {
            EventSpeech = ConEventSpeech(Event);

            if(EventSpeech != None)
            {
                foreach AllActors(class'ScriptedPawn', aScriptedPawn)
                {
                    if(aScriptedPawn.BindName != EventSpeech.SpeakerName && aScriptedPawn.BindName != EventSpeech.SpeakingToName)
                        continue;
                    
                    // Make sure the next actor is placed somewhere new
                    HelpRotator.Yaw += 4096;

                    if(
                        aScriptedPawn != None &&
                        aScriptedPawn != Self &&
                        VSize(aScriptedPawn.Location - Location) > 300
                    )
                    {
                        // Place the actor within a min/max radius of the player
                        while(Radius > 32 && !aScriptedPawn.SetLocation(Location + Normal(Vector(HelpRotator)) * Radius))
                        {
                            HelpRotator.Yaw += 4096;
                            Radius -= 16;
                        }

                        aScriptedPawn.LookAtActor(Self, true, false, true, 0, 0.5);
                        aScriptedPawn.GoToState('Conversation');
                    }
                }
            }

            Event = Event.NextEvent;
        }
    }

    return Super.StartConversation(InvokeActor, InvokeMethod, Con, bAvoidState, bForcePlay);
}

// ----------------------------------------------------------------------
// Make sure the player is given back control after interpolation ends
// ----------------------------------------------------------------------
state Interpolating
{
    ignores all;

    function TakeDamage(int Damage, Pawn instigatedBy, Vector hitlocation, Vector momentum, name damageType) {}
    exec function Fire(optional float F) {}

    event InterpolateEnd(Actor Other)
    {
        if (InterpolationPoint(Other).bEndOfPath)
        {
            if (NextMap != "")
            {
                Level.Game.SendPlayer(Self, NextMap);
            }
            else
            {
                GoToState('PlayerWalking');    
                ShowHud(True);
                foreach AllActors(class'Actor', aActor, 'EndIntroSequence')
                {
                    Event = '';
                    Tag = '';
                    aActor.Trigger(Self, Self);
                    break;
                }
            }
        }
    }

    event PlayerTick(float DeltaTime)
    {
        UpdateInHand();
        ShowHud(False);
    }

Begin:
    if (bOnFire)
        ExtinguishFire();

    bDetectable = False;

    if (Weapon != None)
    {
        Weapon.bHideWeapon = True;
        Weapon = None;
        PutInHand(None);
    }

    if (CarriedDecoration != None)
    {
        CarriedDecoration.Destroy();
        CarriedDecoration = None;
    }

    PlayAnim('Still');
}

// END UE1
