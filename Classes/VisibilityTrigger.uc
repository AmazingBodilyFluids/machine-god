//=============================================================================
// VisibilityTrigger - Toggles an actor's visibility on/off
//=============================================================================
class VisibilityTrigger extends Trigger;

var() bool bVisible;

defaultproperties
{
     bVisible=True
}

// BEGIN UE1

function Trigger(Actor Other, Pawn Instigator)
{
    local Actor A;

    if (Event != '')
    {
        foreach AllActors(class 'Actor', A, Event)
        {
            A.bHidden = !bVisible;
        }
    }

    Super.Trigger(Other, Instigator);
}

// END UE1
