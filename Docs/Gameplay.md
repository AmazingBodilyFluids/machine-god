# Gameplay

This document aims to describe gameplay ideas and principles


## Augmentations

### Possess

Allows the player to control NPCs for a limited amount of time. As the aug is upgraded, the battery drain is reduced.

Uses:  
- Bypassing handprint scanners  
- Bypassing retinal scanner  
- Causing infighting between NPCs  

Activation:  
- Player aims at an NPC and activates the aug
- The NPC is hidden
- Player adopts Mesh, MultiSkins, BindName

Deactivation:  
- NPC is returned
- Player assumes defaultprops again
- NPC faints?


### Telepathy

Allows the player to read the minds of NPCs. As the aug is upgraded, download time is reduced and descramble quality is improved.  

Uses:  
- Discovering keycodes and password
- ...and various other secrets?

Activation:
- Player aims at an NPC and activates the aug  
- Download starts, player can interrupt by deactivating  
- Text is downloaded describing memories of the NPC  
- Static word salad interspersed with useful information  

Decativation:  
- Happens automatically when finished downloading  


## Levels

### Tjuana: Simulation

The player is trapped in a Matrix-like simulated world with Miguel. She will need to help other trapped people awaken to disturb the system enough that it will eject her.