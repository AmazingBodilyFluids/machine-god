# Deus Ex: Machine God


## Abstract

JC Denton has decided to merge with Helios, the international communications protocol and all-knowing AI, extending its reach to the physical realm. The creepy duo is intending to use an army of augmented agents (MiB/WiB) as a means of implementing and enforcing their scheme of nanite powered mental synchronicity and surveillance of the entire human race. Paul Denton and Tracer Tong remain loyal to JC/Helios, convinced that his vision is altruistic in nature.

Gary Savage and his daugher Tiffany become aware of the situation when they have to shield themselves againt JC/Helios' agents as they try to seize the Universal Constructor at the X51 labs. As Gary feels entirely culpable for having made this possible, him being the father of nano-augmentation, he and Tiffany set out to throw a spanner in JC/Helios' machinery by dismantling the network upon which they rely for completing their sinister design. The Savages will, to their dismay, have to rely on the knowledge and resources of the Illuminati, of which Gary is a former member, to carry out the sabotage. Their help will come at a price, of course, in the form of sworn loyalty to the Illuminati's interests.

Players assume the role of a nano-augmemted Tiffany Savage, who will travel across the world to sabotage the global communications network, ultimately plunging the modern world into a great collapse. How (and if) she chooses to do so, is entirely up to her.


## Structure

## 0. Intro

The Vandenberg barracks set the stage. Tiffany lives a somewhat privileged life, but is not averse to danger and challenges. The pretty and polished is contrasted with the cold and practical.


## 1. Ordinary world

The lab is the home base, although the player won't see it again. Here we establish what's normal by showing Gary locked up in his lab. A sense of claustrophobia.


## 2. Call to adventure

The threat of JC/Helios wanting to take the UC and mentally enslave mankind inspires Tiffany to take action. Gary enlsists the help of Tracer Tong. A sense of naïve assertiveness.


## 3. Refusal of the call

A warning by Tong. A sinister threat beyond the known world is alluded to, involving nanites and their capacity to control people.


## 4. Meeting the mentor

Julia Montes is being harassed by the city guard. She turns out to be Mike's mother.

Mike's mother reveals some secrets, helps illicit the help of local gangs. A positive spin on the unknown world.


## 5. Crossing into the special world

A new type of aug? Something about mental control? An alternate world? Use Matrix view/ghost. Illuminati members teach Tiffany and Mike.


## 6. Tests, allies, enemies

Tong regretfully betrays Tiffany, leads her into being captured by Paul Denton and agents. They take her to Hong Kong. Mike is taken elsewhere.


## 7. Approach to the inmost cave

Tiffany escapes captivity and meets JC/Helios as a mental projection being forced upon her. He explains that the merging is incomplete, and what the next stage of progression is. He needed another UC to cultivate nanites and complete the merging, but Tiffany, having learned of her true potential, could now act as an alternative to that method by joining him. He tries to convince Tiffany to join the merging process and help him execute his plan. If she does, the game ends here.


## 8. Ordeal

When Tiffany refuses JC/Helios, he sucks her in forcibly. Overcoming this challenge transforms Tiffany in some way. Depending on her actions here, Mike will live or die. She meets Jock, who helps her leave Hong Kong.


## 9. Reward

How has she changed? What can she do now? Has she changed appearance? Mastery of the skill she was being taught by the Illuminati earlier. An evolution of herself, rebirth. Reunion with Mike, if he survived.


## 10. The chase

JC/Helios has taken the UC and Gary. How is the UC used in game?


## 11. Resurrection

Big battle. Repeated but evolved elements from the ordeal.


## 12. Resolution

Tiffany is given the choice between 2 endings:

1. Kill JC and hand over control of Helios to the Illuminati.
2. Introduce some subtle flaws into JC's nanite architecture so the merging fails, plunging him into a coma and triggering the collapse.


## Mission 16: Lompoc, California (X51 labs)

Augmented agents are storming the X51 labs, this time under JC/Helios' command. The lab contains the other of the only two universal constructors in existence, and JC/Helios considers it too risky to leave it in the hands of anyone who might oppose them. They initially contacted Gary to ask him to hand it over peacefully, but resorted to violence when they were rejected.

The first moments consist of Tiffany, who is staying in the general's quarters of the barracks, being filled in on the situation by Gary. The airbase is under an internal lockdown, because a few agents managed to push their way inside. Her objective is simply to reach him at his laboratory, sneaking or fighting her way past several agents. Gary will comment on her methods once completes this objective. 

JC/Helios appears on the holocomm in front of the Savages, saying that as he cannot currently breach their lockdown, he will make another attempt later. Gary and Tiffany decide to make contact with Morgan Everett of the Illuminati, as they lack the resources and knowledge to defeat JC/Helios. Everett is not easy to contact, though, so Gary says he will attempt to solicit the help of Tracer Tong, as he owes Gary a favour for curing his grey death. Gary entrusts Tiffany to the field work along with their driver, and while she goes to find the driver, Gary manages to contact Tong, who provides the approximate location of Nicolette DuClare, who has set up base in Tijuana while she investigates potential ways to seize the Aquinas Hub at Area 51.


## Mission 17: Slums, Tijuana, Mexico

Tiffany and her driver Mike, travel to Tijuana, and Tiffany is to track down and contact Nicolette. Upon arriving, Tiffany learns that the inner city is restricted to residents only, so she has to explore the slums for a way inside. 

Mike has a mother in Tijuana that can put her in contact with some local gangs, and the Illuminati.

Mike's mother reveals that his real name is Miguel, and they both used to work with the NSF, who funded their own mechanical augmentations. After Juan Lebedev was captured, the NSF's funding was all but eliminated, and their organisation mostly dissolved. Miguel presented himself as Mike and found a job as a driver for Gary, but she stuck around in Tijuana and got to know the cartels that she hated for taking away her husband. She tells Tiffany the password to a gang hideout.

As Tiffany approaches the hideout, she runs into Sandra Renton, who joined the cartel for protection and shelter. She introduces Tiffany to Marcel, the head of the group. He offers Tiffany a job, to get rid of a city guard that is preventing Marcel from carrying out his business of getting people into the city illegally. In exchange, he will see to it that she obtains passage into the city, where she can continue her search for Nicolette.

Tiffany is given a fake ID chip by the cartel, so she can enter the city. This turns out to be a trap, and she finds herself in a simulated miniature world, designed to keep her in stasis as her brain juices are harvested for processing power.

## Mission 18: Simulation, Tijuana, Mexico

Tiffany finds herself arriving by subway to a plaza, where people have lost their memories and are living simulated lives. She was immune to the brain wipe because she's augmented. Her job is now to find Miguel, and when she does, she convinces him that the world is simulated, and he awakens. This causes a glitch and the system reboots. Tiffany and Miguel are now back at the subway, and they devise a plan to awaken more people to destabilise the simulation. As they awaken a significant number of people, Toffany is detected as a virus and reported to the admins. Julia, who works for the Illuminati, discovers Tiffany this way, and pulls her and Miguel out. She says this was a mistake, she should not have seen that. The Illuminati are using human brain power to hack into Helios in order to eventually take control of it. JC stands in the way, though, so they need to remove him first. To that end, they have developed their own nanoaugmentations, a craft they learned from spying on Gary Savage.

Tiffany is given an augmentation to install, which either grants her the ability to take over people's minds or to read them, depending on the player's preference. The Illuminati put her through some simulations in order to teach Tiffany how to use the augs, and she then sets out for Area 51 to confront JC/Helios. Upon eiting the building, she is surrounded by agents and is knocked out.


## Mission 3: Hong Kong (underground tunnel)

She wakes up in detainment in a subterranean tunnel. Paul Denton appears before her and says he's not sure what she's up to, but as he got tipped off by Tong that she was making contact with Illuminati, he didn't want to take any chances. He explains why what they're doing is best for mankind, and that if she would change her mind and join them, she would be on the right side of history. For now, though, she will remain in captivity until they decide what to do with her. As he takes his leave, he lets her know that they don't mean for anything to happen to her, and that she can ask the agents for help if she needs anything. If the player chooses to talk to the agent directly outside their cell, they can plant a seed of doubt in the agent's mind, making them an ally later.

She is contacted through infolink by Alex Jacobson, who, despite his alliance with Tong and the Dentons, has grown suspicious of them due to JC merging with Helios. When he heard that they had detained Tiffany, he was sure they were no longer the idealists he used to know. He lets her know where she is and helps her escape past security systems, agents, maintenance rooms, ventilation systems and environmental hazards. At the end of the level, Alex lets her know about Jock, JC's former pilot, whom she can find in the local bars outside, and wishes her good luck.


## Mission 4: Hong Kong (streets/hub)

When Tiffany emerges from the tunnel, Gary picks up her signal and is relieved that she is alive. She comes across Jock in a bar, and she can get him talking for the price of a beer. He says that he is pretty much set for life with the ridiculously high salary UNATCO paid him, this is just his way of determining kindness in people. Tiffany then informs Jock what JC has been up to, and that she intends to stop him. Jock is astonished, as he thought JC was going to stop Bob Page, not replace him. He feels guilty for having helped carry out this scheme, and bitter for having been left out of the loop. In order to make amends, he will offer his help for free.


