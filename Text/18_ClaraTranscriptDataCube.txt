<P>Interviewee: Clara Estefan
<P>Date: 12/09/2052
<P>Time: 02:15 PM
<P>
<P>[Maria Jose] Let us begin. First I have to disclaim that our interview will be recorded. If you consent to it, we may continue.
<P>[Clara Estefan] Si, I consent.
<P>[Maria Jose] Good. Let us start with your work history - at 27 your resume, I have to say is... kaleidoscopic. 
<P>[Clara Estefan] I started working at 15. 
<P>[Maria Jose] I see. I must say I was still too busy thinking about boy-bands and homework and you were already in… 
<P>[Clara Estefan] Hospitality, Hotel De Pueblo. I started as a dishwasher, thena waiter - at 17 I was recepcionista, and at 18 the manager.
<P>[Maria Jose] Impressive. Your parents must have been proud.
<P>[Clara Estefan] I wouldn't know. 
<P>[Maria Jose] I see, but you do have family? 
<P>[Clara Estefan] My parents fell to the flu pandemic of '42. My brother and I were given board on condition I earned it. As you can I see, we've managed.
<P>[Maria Jose] Indeed. You've then moved into real estate?
<P>[Clara Estefan] Yes, GreenHouse.
<P>[Maria Jose] Still without the benefit of a formal education? 
<P>[Clara Estefan] "Formal" was not a luxury I could afford. 
<P>[Maria Jose] How would you describe your time at GreenHouse?
<P>[Clara Estefan] Fruitful. Though, we've had to move often - after selling two dozen houses, we could afford to buy our own.
<P>[Maria Jose] And then you've "moved" into pharmaceuticals it sauys Versalife here?
<P>[Clara Estefan] My brother contracted gray death.
<P>[Maria Jose] Dios. I'm sorry. 
<P>[Clara Estefan] Don't be, he's doing well, as long as we can afford Ambrosia.
<P>[Maria Jose] About that - you've applied for Hardware QA position, you understand what that job entails?
<P>[Clara Estefan] Testing and feedback on your XR+ tech I assume. 
<P>[Maria Jose] Something like that, yes. And I think you fit this job perfectly.
<P>[Clara Estefan] When can I begin?
<P>[Maria Jose] You were already plugged in before the interview started, miss Estefan.
