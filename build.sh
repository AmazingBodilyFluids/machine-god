#!/bin/sh

# 0. Set up temporary variables
PACKAGE_NAME="MachineGod"
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
RETURN_DIR=$PWD

# 1. Go to the DeusEx/System folder
cd "$HOME/.deus-ex-sdk/drive_c/DeusEx/System"


# 2. Build for Unreal Engine 1
echo "=======================================";
echo "Building for Unreal Engine 1";
echo "=======================================";
echo ""

# 2.1. Remove existing .u files from DeusEx/System
for FILE in $(find . -name "$PACKAGE_NAME*.u")
do
    echo "Removing $FILE"
    rm $FILE
done

# 2.2. Run UE1 compiler
WINEPREFIX="$HOME/.deus-ex-sdk" WINEARCH=win32 wine ./UCC.exe make

# 2.3. Copy built .u files to the DeusEx/MyPackage/System folder
for FILE in $(find . -name "$PACKAGE_NAME*.u")
do
    echo "Copying $FILE -> ../$PACKAGE_NAME/System/$(basename $FILE)"
    cp $FILE "../$PACKAGE_NAME/System"
done

# 3. Build for Unreal Engine 2
echo ""
echo "=======================================";
echo "Building for Unreal Engine 2";
echo "=======================================";
echo ""

# 3.1. Remove existing .u files from DeusEx/UED22
for FILE in $(find ../UED22 -name "$PACKAGE_NAME*.u")
do
    echo "Removing $FILE"
    rm $FILE
done

# 3.2. Comment UE1 exclusive code
for FILE in $(find "$SCRIPT_DIR/Classes" -name '*.uc')
do
    sed -i 's/\/\/ BEGIN UE1/\/\* BEGIN UE1/' "$FILE"
    sed -i 's/\/\/ END UE1/END UE1 \*\//' "$FILE"
done

# 3.3. Run UE2 compiler
WINEPREFIX="$HOME/.deus-ex-sdk" WINEARCH=win32 wine ../UED22/UCC.exe make

# 3.4. Uncomment UE1 exclusive code
for FILE in $(find "$SCRIPT_DIR/Classes" -name '*.uc')
do
    sed -i 's/\/\* BEGIN UE1/\/\/ BEGIN UE1/' "$FILE"
    sed -i 's/END UE1 \*\//\/\/ END UE1/' "$FILE"
done

# 3.5 Copy .u files from DeusEx/UED22 to DeusEx/Ued2 folder
for FILE in $(find ../UED22 -name "$PACKAGE_NAME*.u")
do
    echo "Copying $FILE -> ../Ued2/$(basename $FILE)"
    cp $FILE ../Ued2
done

# 4. Return to the directory we came from
cd $RETURN_DIR
