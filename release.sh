#!/bin/sh

PACKAGE_NAME="MachineGod"
RETURN_DIR=$PWD
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR

VERSION=$(cat ./Package.ini | grep -o '[0-9]\+\.[0-9]\+\.[0-9]\+')

if [ -f "$PACKAGE_NAME-$VERSION.zip" ]
then
    echo "A package for version $VERSION already exists."
else
    rm MachineGod*.zip
    zip -r "$PACKAGE_NAME-$VERSION.zip" ./Maps/*.dx ./System/*.ini ./System/*.u ./Textures/*.utx

    cd $RETURN_DIR
fi
